function toaster(id) {
  document.querySelector(id).show();
}
function dialogue(id) {
  document.querySelector(id).toggle();
}
function link(url) {
  window.location.href = url;
}